<?php

class categoryModel
{

    private $db;

    function __construct()
    {
        $this->db = new PDO('mysql:host=localhost;' . 'dbname=cinemafari;charset=utf8', 'root', '');
    }


    // TRAE LISTA DE CATEGORIAS 
    function getCategoriesFromDB()
    {
        $sentencia = $this->db->prepare("SELECT * FROM genders");
        $sentencia->execute();
        $genders = $sentencia->fetchAll(PDO::FETCH_OBJ);
        return $genders;
    }

    // TRAE UNA CATEGORIA EN PARTICULAR POR ID
    function getCategoryFromDB($id)
    {
        $sentencia = $this->db->prepare("SELECT * FROM genders WHERE genders.id_genre=?");
        $sentencia->execute(array($id));
        $genre = $sentencia->fetch(PDO::FETCH_OBJ);
        return $genre;
    }

    // AGREGA CATEGORIA EN DB
    function createCategoryInDB($name)
    {
        $sentencia = $this->db->prepare("INSERT INTO genders (name) VALUES(?)");
        $sentencia->execute(array($name));
    }

    // ELIMINA CATEGORIA POR ID
    function deleteCategoryFromDB($id)
    {
        $sentencia = $this->db->prepare("DELETE FROM genders WHERE id_genre = ?");
        $sentencia->execute(array($id));
    }

    // EDITA CATEGORIA
    function updateCategoryInDB($id, $name)
    {
        $sentencia = $this->db->prepare("UPDATE genders SET name =? WHERE id_genre =?");
        $sentencia->execute(array($name, $id));
    }
}
