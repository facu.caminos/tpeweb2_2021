<?php

class loginModel
{

    private $db;

    function __construct()
    {
        $this->db = new PDO('mysql:host=localhost;' . 'dbname=cinemafari;charset=utf8', 'root', '');
    }


    // REGISTRA AL USUARIO EN DB
    function registerUser($name, $email, $password)
    {
        $sentencia = $this->db->prepare("INSERT INTO users(name, email, password) VALUES (?, ?, ?)");
        $sentencia->execute(array($name, $email, $password));
    }


    // TRAE USUARIO DE LA DB 
    function getUser($email)
    {
        $sentencia = $this->db->prepare('SELECT * FROM users WHERE email = ?');
        $sentencia->execute([$email]);
        return $sentencia->fetch(PDO::FETCH_OBJ);
    }
}
