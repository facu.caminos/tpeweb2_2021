<?php

class MovieModel
{

    private $db;

    function __construct()
    {
        $this->db = new PDO('mysql:host=localhost;' . 'dbname=cinemafari;charset=utf8', 'root', '');
    }


    // TRAE LA TABLA DE PELICULAS
    function getMoviesFromDB()
    {
        $sentencia = $this->db->prepare("SELECT * FROM movies");
        $sentencia->execute();
        $movies = $sentencia->fetchAll(PDO::FETCH_OBJ);
        return $movies;
    }


    // FILTRA PELICULAS POR CATEGORIA Y ME TRAE TODOS LOS DATOS
    function getMoviesCategoryFromDB($id)
    {
        $sentencia = $this->db->prepare("SELECT * FROM movies, genders WHERE movies.fk_genre = genders.id_genre AND movies.fk_genre=?");
        $sentencia->execute(array($id));
        $moviesByGenre = $sentencia->fetchAll(PDO::FETCH_OBJ);
        return $moviesByGenre;
    }


    // TRAE UNA PELICULA EN PARTICULAR POR ID
    function getMovieFromDB($id)
    {
        $sentencia = $this->db->prepare("SELECT * FROM movies JOIN genders WHERE movies.fk_genre = genders.id_genre AND id_movie = ?");
        $sentencia->execute(array($id));
        $movie = $sentencia->fetch(PDO::FETCH_OBJ);
        return $movie;
    }


    // TRAE LAS DOS TABLAS: PELICULAS Y CATEGORIAS
    function getMovieAndCategoryFromDB()
    {
        $sentencia = $this->db->prepare("SELECT * FROM movies INNER JOIN genders ON movies.fk_genre = genders.id_genre");
        $sentencia->execute();
        $movies = $sentencia->fetchAll(PDO::FETCH_OBJ);
        return $movies;
    }


    // ELIMINA PELICULA POR ID
    function deleteMovieFromDB($id)
    {
        $sentencia = $this->db->prepare("DELETE FROM movies WHERE id_movie = ?");
        $sentencia->execute(array($id));
    }


    // AGREGA PELICULA EN DB
    function createMovieInDB($title, $genre, $description, $rating, $director, $image)
    {
        $sentencia = $this->db->prepare("INSERT INTO movies (title, fk_genre, description, rating, director, image) VALUES(?,?,?,?,?,?)");
        $sentencia->execute(array($title, $genre, $description, $rating, $director, $image));
    }


    // MODIFICA DATOS DE PELICULA EN DB
    function updateMovieInDB($id, $title, $genre, $description, $rating, $director, $image)
    {
        $sentencia = $this->db->prepare("UPDATE movies SET title =?, Fk_genre=?, description=?, rating=?, director=?, image=? WHERE id_movie =?");
        $sentencia->execute(array($title, $genre, $description, $rating, $director, $image, $id));
    }
}
