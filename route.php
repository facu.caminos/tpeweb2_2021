<?php

require_once "Controller/movieController.php";
require_once "Controller/categoryController.php";
require_once "Controller/loginController.php";



define('BASE_URL', '//' . $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . dirname($_SERVER['PHP_SELF']) . '/');

if (!empty($_GET['action']))
    $action = $_GET['action'];
else
    $action = 'home';

$params = explode('/', $action);

$movieController = new movieController();
$categoryController = new categoryController();
$loginController = new loginController();


switch ($params[0]) {
    case 'home':
        $categoryController->getCategories();
        $movieController->getMovies();
        break;

    case 'category':
        $categoryController->getCategories();
        $movieController->getMoviesCategory($params[1]);
        break;

    case 'movie':
        $categoryController->getCategories();
        $movieController->getMovieInfo($params[1]);
        break;

    case 'login':
        $categoryController->getCategories();
        $loginController->getFormLogin();
        break;

    case 'registro':
        $categoryController->getCategories();
        $loginController->getFormRegistro();
        break;

    case 'registrar':
        $loginController->addAdmin();
        break;

    case 'verify':
        $loginController->loginVerify();
        break;

    case 'logout':
        $loginController->logout();
        break;

    case 'setMovies':
        $movieController->setMovies();
        break;

    case 'formCreateMovie':
        $movieController->getFormCreateMovie();
        break;

    case 'createMovie':
        $movieController->createMovie();
        break;

    case 'deleteMovie':
        $movieController->deleteMovie($params[1]);
        break;

    case 'formUpdateMovie':
        $movieController->getFormUpdateMovie($params[1]);
        break;

    case 'updateMovie':
        $movieController->updateMovie($params[1]);
        break;

    case 'setCategories':
        $categoryController->setCategories();
        break;

    case 'formCreateCategory':
        $categoryController->getFormCreateCategory();
        break;

    case 'createCategory':
        $categoryController->createCategory();
        break;

    case 'deleteCategory':
        $categoryController->deleteCategory($params[1]);
        break;

    case 'formUpdateCategory':
        $categoryController->getFormUpdateCategory($params[1]);
        break;

    case 'updateCategory':
        $categoryController->updateCategory($params[1]);
        break;

    default:
        echo ("error 404 not page found");
        break;
}
