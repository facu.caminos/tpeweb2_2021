<?php

require_once "./View/loginView.php";
require_once "./Model/loginModel.php";

class loginController
{

    private $model;
    private $view;

    function __construct()
    {
        $this->model = new loginModel();
        $this->view = new loginView();
    }


    // DESLOGUEA
    function logout()
    {
        session_start();
        session_destroy();
        $this->view->showHomeLocation();
    }



    // TRAE FORMULARIO DE LOGIN
    function getFormLogin()
    {
        $this->view->showFormLogin();
    }


    // TRAE FORMULARIO DE REGISTRO
    function getFormRegistro()
    {
        $this->view->showFormRegistro();
    }


    // REGISTRAR USUARIO
    function addAdmin()
    {
        $name = $_POST['name'];
        $email = $_POST['email'];
        $password = password_hash($_POST['password'], PASSWORD_BCRYPT);

        $this->model->registerUser($name, $email, $password);
        $this->view->showLoginLocation();
    }


    // VERIFICA SI EL USUARIO ESTA REGISTRADO EN DB
    function loginVerify()
    {

        if (!empty($_POST['email']) && !empty($_POST['password'])) {

            $email = $_POST['email'];
            $password = $_POST['password'];

            // Obtengo el usuario de la base de datos
            $user = $this->model->getUser($email);


            // Si el usuario existe y las contraseñas coinciden
            if ($user && password_verify($password, $user->password)) {

                session_start();
                $_SESSION["email"] = $email;

                $this->view->showAdminLocation();
            } else {
                $this->view->showLoginLocation();
                $this->view->showFormLogin("Acceso denegado");
            }
        }
    }
}
