<?php

require_once "./Model/movieModel.php";
require_once "./View/movieView.php";
require_once "./helpers/authHelper.php";

class movieController
{

    private $model;
    private $view;
    private $authHelper;

    function __construct()
    {
        $this->model = new movieModel();
        $this->view = new movieView();
        $this->authHelper = new authHelper();
    }


    // TRAE TODAS LAS PELICULAS
    function getMovies()
    {
        $movies = $this->model->getMoviesFromDB();
        $this->view->showMovies($movies);
    }


    // TRAE PELICULAS POR CATEGORIA
    function getMoviesCategory($id)
    {
        $moviesByGenre = $this->model->getMoviesCategoryFromDB($id);
        if (!empty($moviesByGenre)) {
            $this->view->showMoviesCategory($moviesByGenre);
        } else{
            $this->view->showHomeLocation();
        }
        
    }


    // TRAE UNA PELICULA EN PARTICULAR
    function getMovieInfo($id)
    {
        $movie = $this->model->getMovieFromDB($id);
        $this->view->showMovieInfo($movie);
    }


    // TRAE TEMPLATE MODIFICAR PELICULAS PARA ADMIN
    function setMovies()
    {
        $this->authHelper->checkLoggedIn();

        $movies = $this->model->getMovieAndCategoryFromDB();
        $this->view->showSetMovie($movies);
    }


    // TRAE FORMULARIO CREAR PELICULA
    function getFormCreateMovie()
    {
        $this->authHelper->checkLoggedIn();

        $this->view->showFormCreateMovie();
    }

    // CREAR PELICULA
    function createMovie()
    {
        $this->authHelper->checkLoggedIn();

        $this->model->createMovieInDB($_POST['title'], $_POST['genre'], $_POST['description'], $_POST['rating'], $_POST['director'], $_POST['image']);
        $this->view->showSetMovieLocation();
    }


    // BORRAR PELICULA
    function deleteMovie($id)
    {
        $this->authHelper->checkLoggedIn();

        $this->model->deleteMovieFromDB($id);
        $this->view->showSetMovieLocation();
    }


    // TRAE FORMULARIO PARA EDITAR PELICULA
    function getFormUpdateMovie($id)
    {
        $this->authHelper->checkLoggedIn();

        $movie = $this->model->getMovieFromDB($id);
        $this->view->showFormUpdateMovie($movie);
    }


    // MODIFICAR PELICULA
    function updateMovie($id)
    {
        $this->authHelper->checkLoggedIn();

        $this->model->updateMovieInDB($id, $_POST['title'], $_POST['genre'], $_POST['description'], $_POST['rating'], $_POST['director'], $_POST['image']);
        $this->view->showSetMovieLocation();
    }
}
