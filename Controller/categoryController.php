<?php

require_once "./Model/categoryModel.php";
require_once "./View/categoryView.php";
require_once "./helpers/authHelper.php";

class categoryController
{

    private $model;
    private $view;
    private $authHelper;

    function __construct()
    {
        $this->model = new categoryModel();
        $this->view = new categoryView();
        $this->authHelper = new authHelper();
    }


    // TRAE TODAS LAS CATEGORIAS
    function getCategories()
    {
        $categories = $this->model->getCategoriesFromDB();
        $this->view->showCategories($categories);
    }

    // TRAE TEMPLATE MODIFICAR CATEGORIAS PARA ADMIN
    function setCategories()
    {
        $this->authHelper->checkLoggedIn();

        $categories = $this->model->getCategoriesFromDB();
        $this->view->showSetCategory($categories);
    }

    // TRAE FORMULARIO CREAR CATEGORIA
    function getFormCreateCategory()
    {
        $this->authHelper->checkLoggedIn();

        $this->view->showFormCreateCategory();
    }

    // CREAR CATEGORIA
    function createCategory()
    {
        $this->authHelper->checkLoggedIn();

        $this->model->createCategoryInDB($_POST['name']);
        $this->view->showSetCategoryLocation();
    }

    // BORRAR CATEGORIA
    function deleteCategory($id)
    {
        $this->authHelper->checkLoggedIn();

        $this->model->deleteCategoryFromDB($id);
        $this->view->showSetCategoryLocation();
    }


    // TRAE FORMULARIO DE EDICION DE CATEGORIA
    function getFormUpdateCategory($id)
    {
        $this->authHelper->checkLoggedIn();
        
        $category = $this->model->getCategoryFromDB($id);
        $this->view->showFormUpdateCategory($category);
    }


    // MODIFICA LA CATEGORIA
    function updateCategory($id)
    {
        $this->authHelper->checkLoggedIn();

        $this->model->updateCategoryInDB($id, $_POST['name']);
        $this->view->showSetCategoryLocation();
    }
}
