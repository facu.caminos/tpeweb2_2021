{include file="headerAdmin.tpl"}

<h1>Welcome!</h1>


<table class="table table-striped table-dark">
  <thead>
    <tr>
      <th scope="col">TITLE</th>
      <th scope="col">GENRE</th>
      <th scope="col">DESCRIPTION</th>
      <th scope="col">RATING</th>
      <th scope="col">DIRECTOR</th>
      <th scope="col">IMAGE</th>
      <th scope="col">DELETE</th>
      <th scope="col">UPDATE</th>
    </tr>
  </thead>
  {foreach from=$movies item=$movie}
  <tbody>
    <tr>
      <th scope="row">{$movie->title}</th>
      <td>{$movie->name}</td>
      <td>{$movie->description}</td>
      <td>{$movie->rating}</td>
      <td>{$movie->director}</td>
      <td>{$movie->image}</td>
      <td><a href="{BASE_URL}./deleteMovie/{$movie->id_movie}"><button> delete </button></a></td>
      <td><a href="{BASE_URL}./formUpdateMovie/{$movie->id_movie}"><button> update </button></a></td>

    </tr>
    </tbody>
  {/foreach}
</table>

<a  href="formCreateMovie"> ADD </a>
<a  href="logout"> LOGOUT </a>

  
  
{include file="footer.tpl"}