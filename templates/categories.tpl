
<h1>{$category} movies</h1>

{foreach from=$moviesByGenre item=$movie}
    <div>
        <div class="row">
            <div class="col-xs-12 col-md-4">
                <img class="imgMovie" src="{BASE_URL}./images/movies/{$movie->image}.jpg" alt="">
            </div>
            <div class="col-xs-12 col-md-5">
                <h1>{$movie->title}</h1>
                <p>Genero: {$movie->name}</p>
                <p>Director: {$movie->director}</p>
                <p>Rating: Apta para +{$movie->rating}</p>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <p>Synopsis: {$movie->description}</p>
            </div>
        </div>
    </div>
    <li>
        <hr class="dropdown-divider">
    </li>
{/foreach}

{include file="footer.tpl"}