<form action="{BASE_URL}./updateMovie/{$movie->id_movie}" method="post" class="container">

  <div class="form-group">
    <label for="exampleFormControlInput1">Title</label>
    <input name="title" type="text" class="form-control"  value=" {$movie->title}" id="exampleFormControlInput1" required>
  </div>

  <div class="form-group">
    <label for="exampleFormControlSelect1">Category</label>
    <select name="genre" class="form-control" id="exampleFormControlSelect1" required>
        <option value="1">action</option>
        <option value="2">romance</option>
        <option value="3">science fiction</option>
        <option value="4">comedy</option>
        <option value="5">terror</option>
    </select>
  </div>

  <div class="form-group">
    <label for="exampleFormControlTextarea1">Description</label>
    <textarea name="description" class="form-control" id="exampleFormControlTextarea1" rows="5">{$movie->description}</textarea required>
  </div>

  <div class="form-group">
    <label for="exampleFormControlInput1">Rating</label>
    <input name="rating" type="number" value="{$movie->rating}" class="form-control" id="exampleFormControlInput1" required>
  </div>

  <div class="form-group">
    <label for="exampleFormControlInput1">Director</label>
    <input name="director" type="text"  value=" {$movie->director}"class="form-control" id="exampleFormControlInput1" required>
  </div>

  <div class="form-group">
    <label for="exampleFormControlInput1">Image</label>
    <input name="image" type="text" class="form-control"  value="{$movie->image}" id="exampleFormControlInput1" required>
  </div>

    <button type="submit"> UPDATE </button> 
</form>



{include file="footer.tpl"}