{* MUESTRA EL HOME *}

{* CARRUSEL *}
<div id="carouselExampleIndicators carrusel" class="carousel slide" data-bs-ride="carousel">
    <div class="carousel-indicators">
        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active"
            aria-current="true" aria-label="Slide 1"></button>
        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1"
            aria-label="Slide 2"></button>
        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2"
            aria-label="Slide 3"></button>
    </div>
    <div class="carousel-inner">

        <div class="carousel-item active contieneimgcarrusel">
            <img src="./images/logo/logocarrusel.jpg" class="d-block w-100 imgcarrusel" alt="">
        </div>

        {foreach from=$movies item=$movie}
            <div class="carousel-item contieneimgcarrusel">
                <img src="./images/movies/{$movie->image}.jpg" class="d-block w-100 imgcarrusel" alt="{$movie->title}">
            </div>
        {/foreach}
    </div>
    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators"
        data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators"
        data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
    </button>
</div>
{* /CARRUSEL *}


{* MUESTRA TODAS LAS PELICULAS: IMAGEN Y TITULO *}
<div class="contAllMovies">
    {foreach from=$movies item=$movie}
        <div class="contMovie">
            <img class="imgMovie" src="./images/movies/{$movie->image}.jpg" alt="{$movie->title}">
            <h5><a href="movie/{$movie->id_movie}">{$movie->title}</a></h5>
        </div>
    {/foreach}
</div>


{include file="footer.tpl"}