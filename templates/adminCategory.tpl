{include file="headerAdmin.tpl"}

<h1>Welcome!</h1>


<table class="table table-striped table-dark">
  <thead>
    <tr>
      <th scope="col">TITULO</th>
      <th scope="col">DELETE</th>
      <th scope="col">UPDATE</th>
    </tr>
  </thead>
  {foreach from=$categories item=$category}
  <tbody>
    <tr>
      <th scope="row">{$category->name}</th>
      
      <td><a href="{BASE_URL}./deleteCategory/{$category->id_genre}"><button> delete </button></a></td>
      <td><a href="{BASE_URL}./formUpdateCategory/{$category->id_genre}"><button> update </button></a></td>
    </tr>
    </tbody>
  {/foreach}
</table>

<a  href="formCreateCategory">ADD</a>

  
  
{include file="footer.tpl"}