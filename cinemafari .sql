-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-10-2021 a las 04:05:04
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.1.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `cinemafari`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `genders`
--

CREATE TABLE `genders` (
  `id_genre` int(11) NOT NULL,
  `name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `genders`
--

INSERT INTO `genders` (`id_genre`, `name`) VALUES
(1, 'action'),
(3, 'science fiction'),
(4, 'comedy'),
(5, 'terror'),
(9, 'suspense'),
(10, 'romance');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `movies`
--

CREATE TABLE `movies` (
  `id_movie` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `fk_genre` int(11) NOT NULL,
  `description` varchar(600) NOT NULL,
  `rating` int(11) NOT NULL,
  `director` varchar(200) NOT NULL,
  `image` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `movies`
--

INSERT INTO `movies` (`id_movie`, `title`, `fk_genre`, `description`, `rating`, `director`, `image`) VALUES
(3, 'Cruella', 4, 'Determined to become a successful fashion designer, a young and creative con artist named Estella teams up with a pair of thieves to survive on the streets of London', 13, 'Craig Gillespie', 'Cruella'),
(4, 'The Invisible Man', 5, ' A mad scientist fakes his suicide and then uses his invisibility to terrorize his ex-partner, who decides to confront the invisible man herself after the police did not believe his story.', 18, 'Leigh Whannell', 'TheInvisibleMan'),
(5, '   Insidious', 5, 'Josh (Patrick Wilson), his wife Renai (Rose Byrne) and their three children have just moved into an old house. But, after an unfortunate accident, one of the children falls into a coma and, at the same time, strange phenomena begin to occur in the house that terrify the family.', 18, ' James Wan', ' Insidious'),
(8, ' Mad Max', 3, 'is a 1979 Australian crime action film written and directed by George Miller and starring Mel Gibson.', 16, ' George Miller', ' MadMax'),
(11, 'Cars', 4, 'In a world populated by anthropomorphic talking vehicles, the final race of the Piston Cup season begins a rivalry between retiring veteran Strip \"The King\" Weathers, frequent runner-up Chick Hicks, and brash rookie sensation Lightning McQueen. McQueen, at the back of the field, avoids a multi car crash caused by Chick. While the rest of the running competitors pit for new tires under the pace car, McQueen stays out. This causes him to blow both his rear tires on the last lap. Chick and the King catch up, resulting in a three-way tie. The tiebreaker race is scheduled for one week later at the ', 3, 'John Alan Lasseter', 'Cars'),
(12, 'Bird Box', 9, 'Bird Box is a 2018 American post-apocalyptic horror thriller film directed by Susanne Bier, following a screenplay written by Eric Heisserer, and based on the 2014 novel of the same name by Josh Malerman. The film follows the character Malorie Hayes, played by Sandra Bullock, as she tries to protect herself and two children from entities which cause people who look at them to die by suicide.', 13, 'Susanne Bier', 'BirdBox'),
(14, 'The Happening', 9, 'The Happening is a 2008 psychological thriller film[6] written, co-produced, and directed by M. Night Shyamalan and starring Mark Wahlberg, Zooey Deschanel, John Leguizamo, and Betty Buckley. An Indian-American production, the film follows a group of four as they try to escape an inexplicable natural disaster.', 16, 'M. Night Shyamalan', 'TheHappening'),
(16, 'The Matrix', 3, 'The Matrix is a 1999 science fiction action film[5][6] written and directed by the Wachowskis.[a] It is the first installment in The Matrix film series. Starring Keanu Reeves, Laurence Fishburne, Carrie-Anne Moss, Hugo Weaving, and Joe Pantoliano. It depicts a dystopian future in which humanity is unknowingly trapped inside a simulated reality, the Matrix, which intelligent machines have created to distract humans while using their bodies as an energy source.[7] When computer programmer Thomas Anderson, under the hacker alias \"Neo\", uncovers the truth, he \"is drawn into a rebellion against the', 13, 'Lana Wachowski and Lilly Wachowski', 'TheMatrix'),
(17, 'World War Z', 1, 'World War Z is a 2013 American action horror film directed by Marc Forster, with a screenplay by Matthew Michael Carnahan, Drew Goddard, and Damon Lindelof, from a story by Carnahan and J. Michael Straczynski, based on the 2006 novel of the same name by Max Brooks. The film stars Brad Pitt as Gerry Lane, a former United Nations investigator who travels the world gathering clues to find a way to stop a zombie pandemic.[12] The ensemble supporting cast includes Mireille Enos, Daniella Kertesz, James Badge Dale, Matthew Fox, Ludi Boeken, Fana Mokoena, David Morse, Peter Capaldi, Pierfrancesco Fav', 13, ' Marc Forster', 'WorldWarZ'),
(18, 'Shutter Island', 3, 'Shutter Island is a 2010 American neo-noir psychological thriller film directed by Martin Scorsese and written by Laeta Kalogridis, based on Dennis Lehane\'s 2003 novel of the same name. Leonardo DiCaprio stars as Deputy U.S. Marshal Edward \"Teddy\" Daniels, who is investigating a psychiatric facility on Shutter Island after one of the patients goes missing. Mark Ruffalo plays his partner and fellow deputy marshal, Ben Kingsley is the facility\'s lead psychiatrist, Max von Sydow is a German doctor, and Michelle Williams is Daniels\' wife. Released on February 19, 2010, the film received mostly pos', 13, ' Martin Scorsese', 'Shutter'),
(19, 'The Notebook', 4, 'The Notebook is a 2004 American romantic drama film directed by Nick Cassavetes, written by Jeremy Leven and Jan Sardi, based on the 1996 novel of the same name by Nicholas Sparks. The film stars Ryan Gosling and Rachel McAdams as a young couple who fall in love in the 1940s. Their story is read from a notebook in the present day by an elderly man (played by James Garner), telling the tale to a fellow nursing home resident (played by Gena Rowlands, the director Cassavetes\' mother).', 13, ' Nick Cassavetes', 'TheNotebook'),
(20, 'Titanic', 4, 'Titanic is a 1997 American epic romance and disaster film directed, written, produced, and co-edited by James Cameron. Incorporating both historical and fictionalized aspects, it is based on accounts of the sinking of the RMS Titanic, and stars Leonardo DiCaprio and Kate Winslet as members of different social classes who fall in love aboard the ship during its ill-fated maiden voyage.', 13, 'James Cameron', 'Titanic'),
(21, 'All Good Things', 4, 'All Good Things is a 2010 American mystery/crime romantic drama film directed by Andrew Jarecki and written by Marcus Hinchey and Marc Smerling. Inspired by the life of convicted murderer Robert Durst,[3] it stars Ryan Gosling, Kirsten Dunst, and Frank Langella. Gosling portrays the wealthy son of a New York real estate tycoon (Langella) who develops a volatile relationship with his wife (Dunst) and becomes suspected of a series of murders, as well as his wife\'s unsolved disappearance.', 13, 'Andrew Jarecki', 'AllGoodThings');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(400) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id_user`, `name`, `email`, `password`) VALUES
(1, 'Ariadna Sartuqui', 'arisartuqui@gmail.com', '$2y$10$op4RuNauOVGieiEbJxCGOuAYJj38T6qg88kxGVI4rzLruFTJJWvlS');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `genders`
--
ALTER TABLE `genders`
  ADD PRIMARY KEY (`id_genre`);

--
-- Indices de la tabla `movies`
--
ALTER TABLE `movies`
  ADD PRIMARY KEY (`id_movie`),
  ADD KEY `fk_genre` (`fk_genre`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `genders`
--
ALTER TABLE `genders`
  MODIFY `id_genre` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `movies`
--
ALTER TABLE `movies`
  MODIFY `id_movie` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `movies`
--
ALTER TABLE `movies`
  ADD CONSTRAINT `movies_ibfk_1` FOREIGN KEY (`fk_genre`) REFERENCES `genders` (`id_genre`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
