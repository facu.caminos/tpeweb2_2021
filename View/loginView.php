<?php

class loginView
{

    private $smarty;

    function __construct()
    {
        $this->smarty = new Smarty();
    }


    // MUESTRA FORMULARIO DE LOGIN
    function showFormLogin($error = "")
    {
        $this->smarty->assign("error", $error);
        $this->smarty->display('templates/login.tpl');
    }


    // MUESTRA FORMULARIO DE REGISTRO
    function showFormRegistro()
    {
        $this->smarty->display('templates/registro.tpl');
    }


    function showLoginLocation()
    {
        header("Location: " . BASE_URL . "login");
    }

    function showHomeLocation()
    {
        header("Location: ".BASE_URL."home");
    }

    function showAdminLocation()
    {
        header("Location: " . BASE_URL . "setMovies");
    }
}
