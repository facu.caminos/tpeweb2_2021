<?php
require_once './libs/smarty-3.1.39/libs/Smarty.class.php';


class movieView
{

    private $smarty;

    function __construct()
    {
        $this->smarty = new Smarty();
    }


    //MUESTRA TODAS LAS PELICULAS EN EL 'HOME'
    function showMovies($movies)
    {
        $this->smarty->assign('movies', $movies);

        $this->smarty->display('templates/home.tpl');
    }


    // MUESTRA PELICULAS POR CATEGORIA
    function showMoviesCategory($moviesByGenre)
    {
        $this->smarty->assign('category', $moviesByGenre[0]->name);
        $this->smarty->assign('moviesByGenre', $moviesByGenre);

        $this->smarty->display('templates/categories.tpl');
    }


    // MUESTRA INFO DE UNA PELICULA EN PARTICULAR
    function showMovieInfo($movie)
    {
        $this->smarty->assign('movie', $movie);

        $this->smarty->display('templates/movie.tpl');
    }


    // MUESTRA TEMPLATE MODIFICAR PELICULAS ADMIN
    function showSetMovie($movies)
    {
        $this->smarty->assign('movies', $movies);

        $this->smarty->display('templates/adminMovie.tpl');
    }


    // MUESTRA FORMULARIO CREAR PELICULA
    function showFormCreateMovie()
    {
        $this->smarty->display('templates/createMovie.tpl');
    }


    // MUESTRA FORMULARIO EDITAR PELICULA
    function showFormUpdateMovie($movie)
    {
        $this->smarty->assign('movie', $movie);

        $this->smarty->display('templates/updateMovie.tpl');
    }



    function showSetMovieLocation()
    {
        header("Location: " . BASE_URL . "setMovies");
    }

    function showHomeLocation()
    {
        header("Location: " . BASE_URL . "home");
    }
}
