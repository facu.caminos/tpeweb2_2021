<?php

require_once './libs/smarty-3.1.39/libs/Smarty.class.php';


class categoryView
{
    private $smarty;

    function __construct()
    {
        $this->smarty = new Smarty();
    }


    // MUESTRA LISTADO DE CATEGORIAS EN EL HEADER
    function showCategories($categories)
    {

        $this->smarty->assign('categories', $categories);

        $this->smarty->display('templates/header.tpl');
    }


    // MUESTRA TEMPLATE MODIFICAR CATEGORIA PARA ADMIN
    function showSetCategory($categories)
    {
        $this->smarty->assign('categories', $categories);

        $this->smarty->display('templates/adminCategory.tpl');
    }

    // MUESTRA FORMULARIO CREAR CATEGORIA
    function showFormCreateCategory()
    {
        $this->smarty->display('templates/createCategory.tpl');
    }


    // MUESTRA FORMULARIO EDITAR CATEGORIA
    function showFormUpdateCategory($category)
    {
        $this->smarty->assign('category', $category);

        $this->smarty->display('templates/updateCategory.tpl');
    }



    function showSetCategoryLocation()
    {
        header("Location: " . BASE_URL . "setCategories");
    }
}
